// NOSTL

show_test_parts = false;

wall_thickness = 3;
wall_chamfer = wall_thickness / 4;

carrier_length = 30;
carrier_width = 15;
carrier_height = 15;
