include <libopenscad/mcube.scad>

include <config.scad>

carrier_length = 60;

module carrier_wall() {

    hull() {
        mcube(size = [carrier_length, wall_thickness, carrier_height / 2], center = true, chamfer = wall_chamfer);
        mcube(size = [carrier_length - (carrier_height / 2), wall_thickness, carrier_height], center = true, chamfer = wall_chamfer);
    }
}


module floor() {
    intersection() {
    hull() walls();
    union() {
        translate([0, 0, -(carrier_height - wall_thickness) / 2 ]) {
            mcube(size = [carrier_length - carrier_height * 2, carrier_width, wall_thickness], center = true, chamfer = wall_chamfer);
            for(y = [-1, 1]) {
                translate([0, y * (carrier_width/2),0]) {
                    color("fuchsia") {
                        intersection() {
                            translate([0, 0, wall_thickness]) {
                                mcube(size = [carrier_length - carrier_height *2 , wall_thickness*2, wall_thickness*2], center = true);
                            }
                            mcube(size = [carrier_length - carrier_height * 2- (wall_chamfer * 2) , wall_thickness*2, wall_thickness*2], center = true, chamfer = wall_chamfer*2);
                        }
                    }
                }
            }
        }
    }
}
}



module walls() {
    color("orange") {
        translate([0, -carrier_width / 2, 0]) carrier_wall();
        translate([0,  carrier_width / 2, 0]) carrier_wall();
    }
}



floor();



difference() {
    walls();

    union() {
        for(x = [-1, 1]) {
            translate([x * (carrier_length/2 - carrier_height / 2), 0, 0]) rotate([90, 22.5, 0]) cylinder(d=5, h=carrier_width * 2, center = true, $fn = 8);
        }
    }
}   


