alpha = 22.5;
beta = 67.5;
b = 0.5;

c = b / cos(alpha);
a=sqrt(c*c - b*b);
echo("a,b,c:", a, b, c);


scale = 25.4 * 3;

width = 2;

plate_thickness = 25.4 / 8;
$fn = 100;
/*
$vpt = [4, 2, 12];
$vpr = [60, 0, 360  * $t];
$vpd = 180;
*/

peg_scale = 0.75;
setback_scale = 1.5;

//$t = 01.0;








/*



  |\
  | \
  |  \
  | a \
  | l  \
  | p   \
  | h    \
  | a     \ 
  |        \
b |         \ c
  |          \
  |           \
  |            \
  |             \
  | 90      beta \
  |_______________\ 
        a

reference: a/b/c 4.14/10/10.82

https://www.calculator.net/right-triangle-calculator.html

*/


function a_from_b_c(     b,     c) = sqrt(pow(c,2)                  - pow(b,2));
function a_from_b_alpha( b, alpha) = sqrt(pow(b/cos(alpha), 2)      - pow(b,2));
function a_from_b_beta(  b,  beta) = sqrt(pow(b/sin(beta), 2)       - pow(b, 2));
function a_from_c_alpha( c, alpha) = c * sin(alpha);
function a_from_c_beta(  c,  beta) = sqrt(pow(c, 2)                 - pow(c * sin(beta), 2));

function b_from_a_c(     a,     c) = sqrt(pow(c, 2)                 - pow(a, 2));
function b_from_a_alpha( a, alpha) = sqrt(pow(a/sin(alpha), 2)      - pow(a, 2));
function b_from_a_beta(  a,  beta) = sqrt(pow(a/cos(90 - alpha), 2) - pow(a, 2));
function b_from_c_alpha( c, alpha) = sqrt(pow(c, 2)                 - pow(c * sin(alpha), 2));
function b_from_c_beta(  c,  beta) = c * sin(beta);

function c_from_a_b(     a,     b) = sqrt(pow(a, 2)                 + pow(b, 2));
function c_from_a_alpha( a, alpha) = a/sin(alpha);
function c_from_a_beta(  a,  beta) = a/cos(90 - alpha);
function c_from_b_alpha( b, alpha) = b/cos(alpha);
function c_from_b_beta(  b,  beta) = b/sin(beta);


module test_right_triangle_calculator() {
    a = 4.14;
    b = 10;
    c = 10.82;
    alpha = 22.5;
    beta = 67.5;
    
    echo("a_from_b_c(",     b,     c, ") = ", a_from_b_c(     b,     c));
    echo("a_from_b_alpha(", b, alpha, ") = ", a_from_b_alpha( b, alpha));
    echo("a_from_b_beta(",  b,  beta, ") = ", a_from_b_beta(  b,  beta));
    echo("a_from_c_alpha(", c, alpha, ") = ", a_from_c_alpha( c, alpha));
    echo("a_from_c_beta(",  c,  beta, ") = ", a_from_c_beta(  c,  beta));

    echo("b_from_a_c(",     a,     c, ") = ", b_from_a_c(     a,     c));
    echo("b_from_a_alpha(", a, alpha, ") = ", b_from_a_alpha( a, alpha));
    echo("b_from_a_beta(",  a,  beta," ) = ", b_from_a_beta(  a,  beta));
    echo("b_from_c_alpha(", c, alpha, ") = ", b_from_c_alpha( c, alpha));
    echo("b_from_c_beta(",  c,  beta, ") = ", b_from_c_beta(  c,  beta));

    echo("c_from_a_b(",     a,     b, ") = ", c_from_a_b(     a,     b));
    echo("c_from_a_alpha(", a, alpha, ") = ", c_from_a_alpha( a, alpha));
    echo("c_from_a_beta(",  a,  beta, ") = ", c_from_a_beta(  a,  beta));
    echo("c_from_b_alpha(", b, alpha, ") = ", c_from_b_alpha( b, alpha));
    echo("c_from_b_beta(",  b,  beta, ") = ", c_from_b_beta(  b,  beta));

}

//test_right_triangle_calculator();



module wing() {

    translate([ -1 * (a * scale * setback_scale), 0, 0]) {
        difference() {

            color("blue") {

                linear_extrude(plate_thickness / 2, center = true) {
                    polygon([
                        [            0,         0],
                        [a     * scale,  b * scale],
                        [width * scale,  b * scale],
                        [width * scale, -b * scale],
                        [a     * scale, -b * scale],
                        [            0,          0],
                    
                        ]);
                }
            }
        
             translate([a * scale * setback_scale, 0, 0]) {
                color("red") {
                    cylinder(d = 25.4 * peg_scale, h = plate_thickness * 2, center = true);
                }
            }
        }
    }
}


center_offset = 10;

for(x = [-1, 1]) {
    
    translate([x * (a * scale * setback_scale + center_offset), 0, 0]) { 
        rotate([0, x > 0 ? 0 : 180, x * $t * 22.5]) {
            wing();
        }

        color("orange") {
            cylinder(d = 25.4 * peg_scale, h = plate_thickness * 4, center = true);
        }



    }
}

difference() {
    color("green") cube([center_offset * 2 + 3, 40, 10], center = true);
    for(x = [-1, 1]) {
        
        translate([x * (a * scale * setback_scale + center_offset), 0, 0]) { 

            color("lightgreen") {
                cylinder(d = (a * scale * setback_scale) * 2 * 1.01, h = plate_thickness * 4, center = true);
            }



        }
    }

}



